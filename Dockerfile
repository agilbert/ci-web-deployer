FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
MAINTAINER borja.aparicio.cotarelo@cern.ch

# Install Kerberos client, rsync and ssh
# Install samba client tools to connect via smbclient to DFS filesystem
RUN yum install -y krb5-workstation xrootd-client samba-client rsync openssh-clients && yum clean all

# '/sbin/deploy-dfs' Script that will copy the web contents the the DFS Web site
# '/sbin/deploy-eos' Script that will rsync the output generated to an EOS folder (could be an EOS Web site)
COPY bin/ /sbin/
